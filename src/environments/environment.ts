// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC47D5dhw7ZMbVdBWis6HQomVbC_d1-i2A',
    authDomain: 'admin-app-e005c.firebaseapp.com',
    databaseURL: 'https://admin-app-e005c.firebaseio.com',
    projectId: 'admin-app-e005c',
    storageBucket: 'admin-app-e005c.appspot.com',
    messagingSenderId: '493757002839'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
