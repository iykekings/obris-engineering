export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC47D5dhw7ZMbVdBWis6HQomVbC_d1-i2A',
    authDomain: 'admin-app-e005c.firebaseapp.com',
    databaseURL: 'https://admin-app-e005c.firebaseio.com',
    projectId: 'admin-app-e005c',
    storageBucket: 'admin-app-e005c.appspot.com'
  }
};
