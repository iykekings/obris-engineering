export interface Service {
  name: string;
  id?: string;
  src?: string;
  body: string;
  type?: string;
  data?: {
    imgSrc: string;
    imgSrcSize: string;
  };
  categories?: Category[];
}

export class Category {
  name = '';
  details = '';
}

export class Contact {
  name: string;
  type: string;
  body: string;
  phone: string;
  email: string;
  company?: string;
}
export interface NewsItems {
  heading: string;
  intro: string;
  _id: string;
  content: Content;
}
export interface Content {
  imgSrc?: string;
  body?: string;
}
export interface SingleNews {
  heading: string;
  content: Content;
  _id: string;
}
