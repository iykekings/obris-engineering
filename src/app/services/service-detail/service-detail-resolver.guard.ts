import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';

import { FireService } from '../../fire.service';

@Injectable()
export class ServiceDetailResolver implements Resolve<any> {
  constructor(private fs: FireService, private router: Router) {}

  resolve(
    route: ActivatedRouteSnapshot, state: RouterStateSnapshot
  ): Observable<any> {
    const id = route.paramMap.get('serviceId');

    return this.fs
      .getService(id).valueChanges()
      .pipe(
        tap(data => {
        if (data) {
         return data;
        } else {
          console.log('Service Not found');
          this.router.navigate(['/services']);
        }
      }));
  }
}
