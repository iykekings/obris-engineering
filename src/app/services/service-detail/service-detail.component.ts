import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FireService } from '../../fire.service';
import { Service } from '../../data-model';
import { AngularFireStorage } from '@angular/fire/storage';

export interface ServiceId extends Service {
  id: string;
}

@Component({
  selector: 'app-service-detail',
  templateUrl: './service-detail.component.html',
  styleUrls: ['./service-detail.component.scss']
})
export class ServiceDetailComponent implements OnInit {
  service$: Observable<any>;
  img: Observable<any>;
  cats: Observable<any>;

  constructor(
    private route: ActivatedRoute,
    private fs: FireService,
    private storage: AngularFireStorage
  ) {
    this.service$ = this.route.paramMap.pipe(
      switchMap(params => {
        const id = params.get('serviceId');
        return this.fs.getService(id).valueChanges();
      })
    );

    this.service$.subscribe(data => (this.cats = data.categories));
    this.service$.subscribe(data => {
      const url = data.data.imgSrc;
      this.storage
        .ref(url)
        .getDownloadURL()
        .subscribe(ref => (this.img = ref));
    });
  }

  ngOnInit() {
  }
}
