import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesComponent } from './services.component';

import { ServicesRoutingModule } from './services-routing.module';
import { ServiceDetailComponent } from './service-detail/service-detail.component';
import { MainServiceComponent } from './main/main-service.component';
import { MaterialFeatureModule } from '../material-feature/material-feature.module';
// import { LoadingModule } from '../loading/loading.module';
import { IconModule } from '../icon/icon.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialFeatureModule,
    IconModule,
    ServicesRoutingModule
  ],
  declarations: [
    ServicesComponent,
    ServiceDetailComponent,
    MainServiceComponent
  ]
})
export class ServicesModule {}
