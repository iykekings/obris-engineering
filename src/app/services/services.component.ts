import { Component} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
// import { Router, NavigationStart, NavigationEnd, NavigationCancel } from '@angular/router';
import { Observable } from 'rxjs';
import { FireService } from '../fire.service';
import { map } from 'rxjs/operators';
import { Service } from '../data-model';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent {
  isHandset: Observable<any>;
  isNotHandset: Observable<any>;
   loading: boolean;

  services: Observable<Service[]>;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private fs: FireService
  ) {
    this.services = this.fs.getAllservice();
    this.isHandset = this.breakpointObserver
      .observe(Breakpoints.Handset)
      .pipe(map(obs => obs.matches));

    this.isNotHandset = this.isHandset.pipe(map(matches => !matches));
  }

  // ngAfterViewInit() {
  //   this.router.events
  //     .subscribe((event) => {
  //       if (event instanceof NavigationStart) {
  //         this.loading = true;
  //       } else if (
  //         event instanceof NavigationEnd ||
  //         event instanceof NavigationCancel
  //       ) {
  //         this.loading = false;
  //       }
  //     });
  // }
}
