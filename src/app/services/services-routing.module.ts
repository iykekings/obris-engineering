import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicesComponent } from './services.component';
import { ServiceDetailComponent } from './service-detail/service-detail.component';
import { MainServiceComponent } from './main/main-service.component';
// import { ServiceDetailResolver } from './service-detail/service-detail-resolver.guard';

const serviceRoutes: Routes = [
  {
    path: '',
    component: ServicesComponent,
    children: [
      { path: '', component: MainServiceComponent },
      { path: ':serviceId', component: ServiceDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(serviceRoutes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule {}
