import { Component, TemplateRef } from '@angular/core';
import {
  BreakpointObserver,
  Breakpoints
} from '@angular/cdk/layout';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {
  isHandset: Observable<any>;
  isNotHandset: Observable<any>;
  modalRef: BsModalRef;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private modalService: BsModalService
  ) {
    this.isHandset = this.breakpointObserver
      .observe(Breakpoints.Handset)
      .pipe(map(obs => obs.matches));

    this.isNotHandset = this.isHandset.pipe(map(matches => !matches));
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
