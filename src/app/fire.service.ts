import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { Service } from './data-model';
import { map } from 'rxjs/operators';
// import { shareAndCache } from 'http-operators';

@Injectable({
  providedIn: 'root'
})
export class FireService {
  success = false;
  error = false;
  private serviceCollection: AngularFirestoreCollection<Service>;

  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage
  ) {}

  getAllservice() {
    this.serviceCollection = this.afs.collection<Service>('services');
    return this.serviceCollection.snapshotChanges().pipe(
      map(actions =>
        actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          const imgSrc = data.data.imgSrc;
          const src = this.storage.ref(imgSrc).getDownloadURL();
          return {
            id,
            src,
            ...data
          } as Service;
        })
      ),
      // shareAndCache('obris-services-cache')
    );
  }

  async addService(s) {
    try {
      await this.afs.collection('services').add(s);
      this.success = true;
    } catch (err) {
      this.error = true;
    }
  }
  async addContact(contactForm) {
    try {
      await this.afs.collection('contacts').add(contactForm);
      this.success = true;
    } catch (err) {
      this.error = true;
      console.log(err);
    }
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

  getService(serviceId) {
    return this.afs.doc(`services/${serviceId}`);
  }

  deleteService(serviceId) {
    this.afs.doc(`services/${serviceId}`).delete();
  }

  updateService(serviceId: string, data) {
    this.afs
      .doc(`services/${serviceId}`)
      .update({ data, updatedAt: this.timestamp });
  }

  getDownloadUrl(path) {
    return this.storage.ref(path).getDownloadURL();
  }
}
