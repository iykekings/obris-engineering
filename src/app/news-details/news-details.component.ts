import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { SingleNews } from '../data-model';
import { NewsService } from '../news.service';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.scss']
})
export class NewsDetailsComponent implements OnInit {
  current: Observable<SingleNews>;

  constructor(private ns: NewsService, private route: ActivatedRoute) {
    this.current = this.route.paramMap.pipe(
      switchMap(params => {
        const id = params.get('id');
        return this.ns.getSingleNews(id);
      })
    );
  }

  ngOnInit() {}
}
