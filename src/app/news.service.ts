import { Injectable } from '@angular/core';
import { NewsItems, SingleNews } from './data-model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { shareAndCache } from 'http-operators';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  news: Observable<NewsItems[]>;
  baseUrl = 'https://radiant-thicket-47823.herokuapp.com';

  constructor(private http: HttpClient) {
    this.getAllNews();
  }

  getAllNews() {
    const path = `${this.baseUrl}/oilandgas`;
    this.news = this.http.get<NewsItems[]>(path).pipe(
      // retryExponentialBackoff(5, 1000),
      // keepFresh(60 * 30 * 1000),
      shareAndCache('obris-news-data')
    );
  }

  getSingleNews(id) {
    const path = `${this.baseUrl}/oilandgas/${id}`;
    return this.http.get<SingleNews>(path);
  }
}
