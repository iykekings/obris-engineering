import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialFeatureModule } from './material-feature/material-feature.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LoadingModule } from './loading/loading.module';
import { CarouselComponent } from './index/carousel/carousel.component';

import { NgAisModule } from 'angular-instantsearch';
import { BootstrapWidgetsModule } from './bootstrap-widgets.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HomeDisplayComponent } from './index/home-display/home-display.component';
import { HomeFeaturesComponent } from './index/home-features/home-features.component';
import { FooterComponent } from './footer/footer.component';
import { SearchComponent } from './search/search.component';
import { IndexComponent } from './index/index.component';
import { DropZoneDirective } from './drop-zone.directive';
import { FileSizePipe } from './file-size.pipe';

// Angularfire

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment.prod';

// Font Awesome
import { IconModule } from './icon/icon.module';
// Http
import { HttpClientModule } from '@angular/common/http';
import { NoPageComponent } from './no-page/no-page.component';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    CarouselComponent,
    HomeDisplayComponent,
    HomeFeaturesComponent,
    FooterComponent,
    SearchComponent,
    IndexComponent,
    NoPageComponent,
    DropZoneDirective,
    FileSizePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    NgAisModule.forRoot(),
    BootstrapWidgetsModule,
    IconModule,
    LoadingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    MaterialFeatureModule,
    AngularFireStorageModule,
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
