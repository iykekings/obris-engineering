import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactComponent } from './contact.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialFeatureModule } from '../material-feature/material-feature.module';
import { ReactiveFormsModule } from '@angular/forms';
import { IconModule } from '../icon/icon.module';

const contactRoutes: Routes = [{ path: '', component: ContactComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(contactRoutes),
    ReactiveFormsModule,
    MaterialFeatureModule,
    IconModule
  ],
  exports: [RouterModule],
  declarations: [ContactComponent]
})
export class ContactModule {}
