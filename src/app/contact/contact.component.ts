import { Component, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Contact } from '../data-model';
import { FireService } from '../fire.service';

export interface Choice {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnChanges {
  contactForm: FormGroup;
  loading = false;
  success = false;
  error = false;

  choices: Choice[] = [
    { value: 'Service-Required', viewValue: 'Service Required' },
    { value: 'Complaints', viewValue: 'Complaints' },
    { value: 'Questions', viewValue: 'Questions' },
    { value: 'Others', viewValue: 'Others' }
  ];

  constructor(private fb: FormBuilder, private fs: FireService) {
    this.createForm();
  }

  createForm() {
    this.contactForm = this.fb.group({
      name: '',
      type: '',
      body: '',
      phone: '',
      email: '',
      company: ''
    });
  }

  ngOnChanges() {
    this.rebuildForm();
  }

  rebuildForm() {
    this.contactForm.reset({
      name: '',
      type: '',
      body: '',
      phone: '',
      email: '',
      company: ''
    });
    this.loading = false;
  }

  onSubmit() {
    this.save(this.prepareSaveContact());
    this.rebuildForm();
  }

  prepareSaveContact(): Contact {
    this.loading = true;
    const formModel = this.contactForm.value;

    const saveContact = {
      name: formModel.name as string,
      type: formModel.type as string,
      body: formModel.body as string,
      phone: formModel.phone as string,
      email: formModel.email as string,
      company: formModel.company as string,
      createdAt: this.fs.timestamp
    };
    return saveContact;
  }

  revert() {
    this.rebuildForm();
  }

  save(c: Contact) {
    console.log(this.success);
    this.fs.addContact(c);
    this.success = this.fs.success;
    this.error = this.fs.error;
    console.log(this.success);

    this.loading = false;
  }

  hide() {
    this.loading = false;
    this.success = false;
    this.error = false;
  }
}
