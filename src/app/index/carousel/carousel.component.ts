import { Component } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent {
  data = [
    { image: 'assets/img/slide/architecture.jpg', title: 'Creativity in architecture'},
    { image: 'assets/img/slide/building.jpg', title: 'Just the Best Building'},
    { image: 'assets/img/slide/circuit.jpg', title: 'Electronics and Electricals'},
    { image: 'assets/img/slide/solar.jpg', title: 'Green Energy Solutions'},
    { image: 'assets/img/slide/tractor.jpg', title: 'Best Yield in Agriculture'}
  ];

  constructor() {
  }
}
