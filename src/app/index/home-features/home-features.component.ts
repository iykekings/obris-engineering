import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-features',
  templateUrl: './home-features.component.html',
  styleUrls: ['./home-features.component.scss']
})
export class HomeFeaturesComponent implements OnInit {
  features = [];

  featuredIcons = [
    'bolt',
    'plane-departure',
    'user-shield',
    'cart-plus',
    'user-md'
  ];

  featureTitles = [
    'Fast delivery',
    'Global',
    'Reliable',
    'Cost Effective',
    'Customer Care'
  ];

  // tslint:disable-next-line:max-line-length
  featureBody = [
    'Our delivery process is highly automated and efficient.',
    'We are all over the world, distance is not a barrier',
    'Our products are built and test by trusted specialised enterprises',
    'Because we get directly from the manufacturer, our products are placed for the best prices to suit you',
    '24/7 trained Customer Care staffs to respond to your questions and complaints'
  ];

  constructor() {}

  ngOnInit() {
    for (let i = 0; i < this.featuredIcons.length; i++) {
      this.features.push({
        icon: this.featuredIcons[i],
        title: this.featureTitles[i],
        body: this.featureBody[i]
      });
    }
  }
}
