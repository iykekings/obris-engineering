import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FireService } from '../../fire.service';
import { Service } from '../../data-model';

@Component({
  selector: 'app-home-display',
  templateUrl: './home-display.component.html',
  styleUrls: ['./home-display.component.scss']
})
export class HomeDisplayComponent implements OnInit {
  services$: Observable<Service[]>;

  constructor(private fs: FireService) {}

  ngOnInit() {
    this.services$ = this.fs.getAllservice();
  }
}
