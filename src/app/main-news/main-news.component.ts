import { Component } from '@angular/core';
import { NewsService } from '../news.service';
import { NewsItems } from '../data-model';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-main-news',
    templateUrl: './main-news.component.html',
    styleUrls: ['./main-news.component.scss']
})
export class MainNewsComponent {
    news: Observable<NewsItems[]>;

    constructor(private ns: NewsService) {
        this.news = this.ns.news;
    }
}
