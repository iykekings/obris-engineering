import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// ngx bootstrap impports
import { CarouselModule } from 'ngx-bootstrap/carousel';

@NgModule({
  imports: [CommonModule],
  exports: [CarouselModule]
})
export class BootstrapWidgetsModule {}
