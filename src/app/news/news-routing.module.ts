import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsComponent } from './news.component';
import { MainNewsComponent } from '../main-news/main-news.component';
import { NewsDetailsComponent } from '../news-details/news-details.component';

const newsRoutes: Routes = [
  {
    path: '',
    component: NewsComponent,
    children: [
      { path: '', component: MainNewsComponent },
      { path: ':id', component: NewsDetailsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(newsRoutes)],
  exports: [RouterModule]
})
export class NewsRoutingModule {}
