import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsComponent } from './news.component';

import { NewsDetailsComponent } from '../news-details/news-details.component';
import { MainNewsComponent } from '../main-news/main-news.component';
import { NewsRoutingModule } from './news-routing.module';
// import { LoadingModule } from '../loading/loading.module';

@NgModule({
  imports: [CommonModule, NewsRoutingModule],
  declarations: [NewsComponent, NewsDetailsComponent, MainNewsComponent]
})
export class NewsModule {}
